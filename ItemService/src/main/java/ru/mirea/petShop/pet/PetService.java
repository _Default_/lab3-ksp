
package ru.mirea.petShop.pet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PetService {

    PetDAO petDataAcsesObject;

    @Autowired
    public PetService(PetDAO petDataAcsesObject) {
        this.petDataAcsesObject = petDataAcsesObject;
    }

    @Nullable
    public List<Pet> findAll() {
        return petDataAcsesObject.findAll();
    }

    @Nullable
    public List<Pet> findPetByID(int id) {
        return petDataAcsesObject.findPetByID(id);
    }

}



