package ru.mirea.petShop.cart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.mirea.petShop.Objects.Item;

import java.util.List;

@Controller
public class CartController {

    @Autowired
    private CartService cartService;

    @RequestMapping(value = "cart", method = RequestMethod.GET)
    @ResponseBody
    public List<Item> getItems() {
        return cartService.items();
    }

    @RequestMapping(value = "cart/pet/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public String putPet(@PathVariable String id) {
        cartService.putPet(Integer.parseInt(id));
        return "ok";
    }


    @RequestMapping(value = "cart/staff/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public String putStaff(@PathVariable int id) {
        cartService.putStaff(id);
        return "ok";
    }

    @RequestMapping(value = "cart/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteItem(@PathVariable int id) {
        cartService.deleteItem(id);
        return "ok";
    }

    @RequestMapping(value = "cart", method = RequestMethod.POST)
    @ResponseBody
    public String buyItems() {
        return cartService.buy();
    }


}
