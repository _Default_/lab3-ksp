package ru.mirea.petShop.cart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.mirea.petShop.Objects.Item;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class CartDAO {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public CartDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Item> findAll() {
        return jdbcTemplate.query("select * from Carts", new RowMapper<Item>(){

            @Override
            public Item mapRow(ResultSet resultSet, int i) throws SQLException {
                int id = resultSet.getInt("id");
                int userId = resultSet.getInt("userId");
                int itemId = resultSet.getInt("itemId");
                return new Item(id,userId,itemId);
            }
        });
    }

    public void addItem(int id, double price) {
        jdbcTemplate.execute("insert into Carts(UserID, ItemId) values(1, " + id + ")");

        jdbcTemplate.execute("update totalprices set cartprice = (select cartprice + " + price +" from totalprices where userid = " + id + ") where userid = " + id);
    }

    public void deleteItem(int id) {
        jdbcTemplate.execute("delete from Carts where Id =" + id);
    }

    public void deleteAll(int id) {
        jdbcTemplate.execute("delete from Carts");
        jdbcTemplate.execute("update totalprices set cartprice = 0 where userid = " + id);
    }

    public double getTotalPrice(int id) {
        String query = "SELECT * FROM TOTALPRICES where userid = " + id;

        List<Price> totalPrice = jdbcTemplate.query(query, new RowMapper<Price>() {

            @Override
            public Price mapRow(ResultSet resultSet, int i) throws SQLException {
                return new Price(resultSet.getInt("cartPrice"));
            }
          });

        return totalPrice.get(0).money;
    }

    class Price{
        double money;

        Price(double money){
            this.money = money;
        }
    }

}
