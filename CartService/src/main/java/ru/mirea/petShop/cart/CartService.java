package ru.mirea.petShop.cart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.mirea.petShop.Objects.Balance;
import ru.mirea.petShop.Objects.Item;
import ru.mirea.petShop.Objects.Pet;
import ru.mirea.petShop.Objects.Staff;

import java.util.List;

@Component
public class CartService {


    private CartDAO cartDataAccessObject;

    @Autowired
    public CartService(CartDAO cartDataAccessObject) {
        this.cartDataAccessObject = cartDataAccessObject;
    }

    public void putPet(int id) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Pet[]> pet = restTemplate.getForEntity("http://localhost:8081/pet/?", Pet[].class, id);

        if (pet.getBody().length != 0) {
            cartDataAccessObject.addItem(id, pet.getBody()[0].getPrice());
        }
    }

    public void putStaff(int id) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Staff[]> staff = restTemplate.getForEntity("http://localhost:8081/staff/", Staff[].class);

        if (staff.getBody().length != 0) {
            cartDataAccessObject.addItem(id, staff.getBody()[0].getPrice());
        }
    }

    public void deleteItem(int id) {
        cartDataAccessObject.deleteItem(id);
    }

    public List<Item> items() {
        return cartDataAccessObject.findAll();
    }

    public String buy() {
        double totalPrice = cartDataAccessObject.getTotalPrice(1);

        RestTemplate restTemplate = new RestTemplate();
        Balance balance = restTemplate.getForObject("http://localhost:8083/balance/", Balance.class);


        if (balance.getMoney() < totalPrice) {
            return "Fail! Small balance!";
        }

        double newBalance = balance.getMoney() - totalPrice;
        restTemplate.postForObject("http://localhost:8083/balance/" + newBalance, "", Balance.class);
        cartDataAccessObject.deleteAll(1);
        return "OK";
    }


}