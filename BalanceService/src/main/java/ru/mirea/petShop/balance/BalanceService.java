package ru.mirea.petShop.balance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BalanceService {

    BalanceDAO balanceDataAccessObject;

    @Autowired
    public BalanceService(BalanceDAO balanceDataAccessObject){
        this.balanceDataAccessObject = balanceDataAccessObject;
    }

    public Balance balance() {
        return balanceDataAccessObject.getBalanceByUserID(1).get(0);
    }

    public void updateUserBalance(double newBalance) {
        balanceDataAccessObject.updateUserBalance(newBalance, 1);
    }
}
