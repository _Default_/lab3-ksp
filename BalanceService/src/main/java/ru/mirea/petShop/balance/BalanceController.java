package ru.mirea.petShop.balance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class BalanceController {

    @Autowired
    private BalanceService balanceService;

    @RequestMapping(value = "balance", method = RequestMethod.GET)
    @ResponseBody
    public Balance getBalance ()
    {
        return balanceService.balance();
    }

    @RequestMapping(value = "balance/{newBalance}", method = RequestMethod.POST)
    @ResponseBody
    public void getBalance (@PathVariable double newBalance)
    {
        balanceService.updateUserBalance(newBalance);
    }
}
